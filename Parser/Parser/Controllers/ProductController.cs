﻿using Microsoft.Office.Interop.Excel;
using Parser.Models;
using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.IO;
using System.Linq;
using OfficeOpenXml;
using System.Runtime.InteropServices;
using System.Threading;
using System.Web;
using System.Web.Mvc;
using DocumentFormat.OpenXml.Packaging;
using DocumentFormat.OpenXml.Wordprocessing;

namespace Parser.Controllers
{
	public class ResultImport
	{
		public List<CategoryFromXls> CategoryList { get; set; }		
	}



	public class ProductController : Controller
    {

	    int counter = 0;

		int startColumnSub = 2;

		// GET: Product
		public ActionResult Index()
        {
            return View();
        }

		public static string ConvertXLS_XLSX(FileInfo file)
		{
			var app = new Microsoft.Office.Interop.Excel.Application();
			var xlsFile = file.FullName;
			var wb = app.Workbooks.Open(xlsFile);
			var xlsxFile = xlsFile + "x";
			if (System.IO.File.Exists(xlsxFile))
			{
				System.IO.File.Delete(xlsxFile);
			}
			wb.SaveAs(Filename: xlsxFile, FileFormat: Microsoft.Office.Interop.Excel.XlFileFormat.xlOpenXMLWorkbook);
			wb.Close();
			app.Quit();
			return xlsxFile;
		}

		public List<ProductGroup> groupList = new List<ProductGroup>();

		public List<CategoryFromXls> allCategory = new List<CategoryFromXls>();
		

		public List<ProductGroupSub> productsReadyForUnloading = new List<ProductGroupSub>();

		public void FileProcessing(int id, string nameFile, FileInfo fileInfo, List<Product> products)
		{
			
			try
			{
				using (FileStream fs =
					System.IO.File.OpenRead(Server.MapPath("~/Content/" + nameFile)))
				{
					using (ExcelPackage excelPackage = new ExcelPackage(fs))
					{
						ExcelWorkbook WorkBook = excelPackage.Workbook;
						ExcelWorksheet Worksheet = WorkBook.Worksheets.First();

						var productGroupSub = new ProductGroupSub()
						{
							Title = nameFile,
							Products = new List<Product>()
						};

						//TODO: В ОТДЕЛЬНУЮ ФУНКЦИЮ!




						for (int rowSub = 3; rowSub <= Worksheet.Dimension.Rows; rowSub++)
						{
							var VendorСode = (Worksheet.Cells[rowSub, startColumnSub]).Text;
							var VolumeAmount = (Worksheet.Cells[rowSub, startColumnSub + 1]).Text;
							var VolumeAmountWithPrices = (Worksheet.Cells[rowSub, startColumnSub + 2]).Text;
							var NumberOfOffersTotal = (Worksheet.Cells[rowSub, startColumnSub + 3]).Text;
							var NumberOfOffers = (Worksheet.Cells[rowSub, startColumnSub + 4]).Text;
							var PriceWeightedByFactor = (Worksheet.Cells[rowSub, startColumnSub + 5]).Text;
							var PriceAverageTakingIntoAccountCoeff = (Worksheet.Cells[rowSub, startColumnSub + 6]).Text;

							if (string.IsNullOrWhiteSpace(VendorСode))
							{
								break;
							}

							var product = new Product()
							{
								VendorСode = VendorСode,
								VolumeAmount = VolumeAmount,
								VolumeAmountWithPrices = VolumeAmountWithPrices,
								NumberOfOffersTotal = NumberOfOffersTotal,
								NumberOfOffersSimple = NumberOfOffers,
								PriceWeightedByFactor = PriceWeightedByFactor,
								PriceAverageTakingIntoAccountCoeff = PriceAverageTakingIntoAccountCoeff,
								ProductCharacteristics = new List<CharacteristicInFile>()
							};
							if (nameFile.EndsWith("00.xlsx"))
							{
								product.ProductWasDownloadedFromFile00 = "1";
							}

							products.Add(product);

							for (int Column = startColumnSub + 7; Column < 100; Column++)
							{
								var currentTitleCharacteristic = (Worksheet.Cells[2, Column]).Text;

								if (string.IsNullOrWhiteSpace(currentTitleCharacteristic))
								{
									break;
								}
								var currentCharacteristic = (Worksheet.Cells[rowSub, Column]).Text;
								var characteristic = new CharacteristicInFile()
								{
									Title = currentTitleCharacteristic,
									productCharacteristics = currentCharacteristic
								};

								product.ProductCharacteristics.Add(characteristic);

							}
							productGroupSub.Products.Add(product);
						}



					}
				}
			}
			catch
			{
				List<Errorss> errors1 = new List<Errorss>();
				var categoryErrors = new Errorss()
				{
					Error = ErrorTypes.NoExistFile
				};
				errors1.Add(categoryErrors);
			}
			
			
			
		}

		public void FileProcessingAdditional(int id, string nameFile, FileInfo fileInfo, List<Product> products, ProductGroupSub productGroupSub)
		{

			try
			{
				using (FileStream fs =
					System.IO.File.OpenRead(Server.MapPath("~/Content/" + nameFile)))
				{
					using (ExcelPackage excelPackage = new ExcelPackage(fs))
					{
						ExcelWorkbook WorkBook = excelPackage.Workbook;
						ExcelWorksheet Worksheet = WorkBook.Worksheets.First();


						foreach (Product item in productGroupSub.Products)
						{
							for (int Column = startColumnSub + 3; Column < 100; Column++)
							{
								var currentTitleCharacteristic = (Worksheet.Cells[2, Column]).Text;
								if (string.IsNullOrWhiteSpace(currentTitleCharacteristic))
								{
									break;
								}
								var productCharacteristics = new CharacteristicInFile()
								{
									Title = currentTitleCharacteristic

								};
								item.ProductCharacteristics.Add(productCharacteristics);
							}
						}


						for (int rowSub = 3; rowSub <= Worksheet.Dimension.Rows; rowSub++)
						{
							for (int Column = startColumnSub + 3; Column < 100; Column++)
							{
								var currentVendorСode = (Worksheet.Cells[rowSub, 2]).Text;
								var volume = (Worksheet.Cells[rowSub, startColumnSub + 1]).Text;
								var numberOfOffersAdditional = (Worksheet.Cells[rowSub, startColumnSub + 2]).Text;
								var currentTitleCharacteristic = (Worksheet.Cells[2, Column]).Text;
								var currentCharacteristic = (Worksheet.Cells[rowSub, Column]).Text;

								if (string.IsNullOrWhiteSpace(currentTitleCharacteristic))
								{
									break;
								}

								foreach (Product item in productGroupSub.Products)
								{
									if (item.VendorСode == currentVendorСode)
									{
										productGroupSub.Products.Select(c => c.Volume);
										item.Volume = volume;
										item.NumberOfOffersAdditional = numberOfOffersAdditional;
										item.ProductCharacteristics.First(c => c.Title == currentTitleCharacteristic).productCharacteristics = currentCharacteristic;
									}
								}
							}


						}
					}
				}
			}
			catch
			{
				
			}

		}

		public CategoryFromXls GetGroup(ExcelWorksheet Worksheet, int row, int categoryIdColumn, int categoryTitleColumn, string parentId="")
		{
			var categoryIdValue = Worksheet.Cells[row, categoryIdColumn].Text;
			var categoryTitleValue = Worksheet.Cells[row, categoryTitleColumn].Text;

            var hereIsGroupInCell = !string.IsNullOrEmpty(categoryIdValue) || !string.IsNullOrEmpty(categoryTitleValue);
			if (hereIsGroupInCell)
			{
				var categoryFromXls = new CategoryFromXls()
				{
					Id = categoryIdValue,
					Title = categoryTitleValue,
					ParentId = parentId
				};
				
				return categoryFromXls;
			}
			else
			{
				return null;
			}

		}

		public void SetProductCategory(ExcelWorksheet Worksheet, ProductGroup productGroup, int row)
		{
			var startCategoryColumn = 7;
			var categoryId = Worksheet.Cells[row, startCategoryColumn + 2].Text;
			var categoryTitle = Worksheet.Cells[row, startCategoryColumn + 3].Text;

           

            var category = GetGroup(Worksheet, row, startCategoryColumn, startCategoryColumn + 1);

			if (category == null)
			{
                var categoryErrors = new Errorss()
                {
                    Error = ErrorTypes.NoCategoryInGroup + String.Format(" в строке: {0}", row)
				};      
                productGroup.Errors.Add(categoryErrors);
            }
			else
			{
				var categoryList = new CategoryFromXls()
				{
					Id = category.Id,
					Title = category.Title
                };

				if (category.Id == "")
				{
					var categoryErrors = new Errorss()
					{
						Error = ErrorTypes.NoIdInCategory + String.Format(" в строке: {0} | в колонке: {1}", row, startCategoryColumn)
					};
					productGroup.Errors.Add(categoryErrors);
				}

				if (category.Title == "")
				{
					var categoryErrors = new Errorss()
					{
						Error = ErrorTypes.NoTitleInCategory + String.Format(" в строке: {0} | в колонке: {1}", row, startCategoryColumn)
					};
					productGroup.Errors.Add(categoryErrors);
				}
				
				if (category.Title != "" && category.Id != "")
				{
					if (!allCategory.Any(x => x.Id == category.Id) && !allCategory.Any(x => x.Title == category.Title))
					{
						allCategory.Add(categoryList);
					}
				}
				
				productGroup.CategoryList.Add(categoryList);
				productGroup.ParentCategoryId = categoryList.Id;
				productGroup.ParentCategoryTitle = categoryList.Title;

				var startSubCategoryColumn = 9;
				while (startSubCategoryColumn < 16)
                {       			
					var category1 = GetGroup(Worksheet, row, startSubCategoryColumn, startSubCategoryColumn + 1, category.Id);        
					if (category1 != null)
					{ 
                        categoryList = new CategoryFromXls()
                        {
                            Id = category1.Id,
                            Title = category1.Title,
							ParentId = category1.ParentId
                        };
                        productGroup.CategoryList.Add(categoryList);

						if (category1.Id == "")
						{
							var categoryErrors = new Errorss()
							{
								Error = ErrorTypes.NoIdInCategory + String.Format(" в строке: {0} | в колонке: {1}", row, startSubCategoryColumn)
							};
							productGroup.Errors.Add(categoryErrors);
						}
						else
						{
							productGroup.ParentCategoryId = categoryList.Id;
						}

						if (category1.Title == "")
						{
							var categoryErrors = new Errorss()
							{
								Error = ErrorTypes.NoTitleInCategory + String.Format(" в строке: {0} | в колонке: {1}", row, startSubCategoryColumn)
							};
							productGroup.Errors.Add(categoryErrors);
						}
						else
						{
							productGroup.ParentCategoryTitle = categoryList.Title;
						}

						if (category1.Title != "" && category1.Id != "")
						{
							if (!allCategory.Any(x => x.Id == category1.Id) && !allCategory.Any(x => x.Title == category1.Title))
							{
								allCategory.Add(categoryList);
							}
						}

						category.Id = category1.Id;
						
					}
					startSubCategoryColumn = startSubCategoryColumn + 2;
				}
			}
		}
		[HttpPost]
		public ActionResult Import(HttpPostedFileBase excelfile)
		{

			if (excelfile == null || excelfile.ContentLength == 0)
			{
				ViewBag.Error = "Select excel file";
				return View("Index");
			}
			else
			{
				if (excelfile.FileName.EndsWith(".xls") || excelfile.FileName.EndsWith(".xlsx"))
				{
					using (FileStream fs =
					System.IO.File.OpenRead(Server.MapPath("~/Content/" + excelfile.FileName)))
					{
						using (ExcelPackage excelPackage = new ExcelPackage(fs))
						{
							ExcelWorkbook WorkBook = excelPackage.Workbook;
							ExcelWorksheet Worksheet = WorkBook.Worksheets.First();

							var startRow = 3;
							var startColumn = 1;

							var endRow = Worksheet.SelectedRange.Count() ;

							for (int row = 3; row <= Worksheet.Dimension.Rows; row++)
							{
								var groupId = (Worksheet.Cells[row, startColumn]).Text;
								var groupIsManualLoading = (Worksheet.Cells[row, startColumn + 1]).Text;
								var groupTitle = (Worksheet.Cells[row, startColumn + 2]).Text;
								var characteristicId = (Worksheet.Cells[row + 1, startColumn + 3]).Text;
								var characteristicTitle = (Worksheet.Cells[row + 1, startColumn + 4]).Text;
								var categoryId = (Worksheet.Cells[row, startColumn + 6]).Text;
								var categoryTitle = (Worksheet.Cells[row, startColumn + 7]).Text;
								var nextGroupId = (Worksheet.Cells[row + 1, startColumn]).Text;
								var nextGroupTitle = (Worksheet.Cells[row + 1, startColumn + 2]).Text;
								

								if (string.IsNullOrWhiteSpace(groupId) && string.IsNullOrWhiteSpace(groupIsManualLoading) && string.IsNullOrWhiteSpace(groupTitle) && string.IsNullOrWhiteSpace(characteristicId) && string.IsNullOrWhiteSpace(characteristicTitle) && string.IsNullOrWhiteSpace(categoryId) && string.IsNullOrWhiteSpace(categoryTitle) && string.IsNullOrWhiteSpace(nextGroupId) && string.IsNullOrWhiteSpace(nextGroupTitle))
								{
									break;
								}

								if (string.IsNullOrWhiteSpace(groupId) && string.IsNullOrWhiteSpace(groupTitle) && string.IsNullOrWhiteSpace(groupIsManualLoading))
								{
									continue;
								}

								//если строчка имеет id группы или ее название

								var productGroup = new ProductGroup()
								{
									ProductGroupId = groupId,
									IsManualLoading = groupIsManualLoading,
									ProductGroupTitle = groupTitle,
									Errors = new List<Errorss>(),
									CategoryList = new List<CategoryFromXls>()
								};
								//TODO: сделать проверку на пустой id или title



								if (groupId == "")
								{
									var categoryErrors = new Errorss()
									{
										Error = ErrorTypes.NoIdInGroup + String.Format(" в строке: {0} | в колонке: {1}", row, startColumn)
									};
									productGroup.Errors.Add(categoryErrors);
								}

								if (groupIsManualLoading != "")
								{
									var categoryErrors = new Errorss()
									{
										Error = ErrorTypes.IsManualUploading + String.Format(" в строке: {0} | в колонке: {1}", row, startColumn + 1)
									};
									productGroup.Errors.Add(categoryErrors);
								}

								if (groupTitle == "")
								{
									var categoryErrors = new Errorss()
									{
										Error = ErrorTypes.NoTitleInGroup + String.Format(" в строке: {0} | в колонке: {1}", row, startColumn + 2)
									};
									productGroup.Errors.Add(categoryErrors);
								}

								groupList.Add(productGroup);

								productGroup.CharacteristicList = new List<ProductCharacteristic>();

								SetProductCategory(Worksheet, productGroup, row);

								//TODO: Возможно нужен else

								var characteristicRow = row + 1;
								var L4 = Worksheet.Cells[characteristicRow, 4].Text;
								while (!string.IsNullOrWhiteSpace(L4))
								{
									var L5 = Worksheet.Cells[characteristicRow + 1, 4].Text;

									var productCharacteristic = new ProductCharacteristic()
									{
										Id = Worksheet.Cells[characteristicRow, startColumn + 3].Text,
										Title = Worksheet.Cells[characteristicRow, startColumn + 4].Text,
										IsAdditional = Worksheet.Cells[characteristicRow, startColumn + 5].Text == "1"
									};

									if (productCharacteristic.Id == "")
									{
										var categoryErrors = new Errorss()
										{
											Error = ErrorTypes.NoIdInCharacteristic + String.Format(" в строке: {0} | в колонке: {1}", characteristicRow, startColumn + 3)
										};
										productGroup.Errors.Add(categoryErrors);
									}
									if (productCharacteristic.Title == "")
									{
										var categoryErrors = new Errorss()
										{
											Error = ErrorTypes.NoTitleInCategory + String.Format(" в строке: {0} | в колонке: {1}", characteristicRow, startColumn + 4)
										};
										productGroup.Errors.Add(categoryErrors);
									}

									productGroup.CharacteristicList.Add(productCharacteristic);
									if (!string.IsNullOrWhiteSpace(L5))
									{
										//TODO: сделать проверку на пустой id или title
										characteristicRow++;
									}
									else
									{
										break;
									}

								}




							}
						}
					}

					

					
					
					DirectoryInfo directoryInfo = new DirectoryInfo(Server.MapPath("~/Content/"));

					for (int id = 0; id < groupList.Count; id++)
					{
						if(groupList[id].CategoryList != null && groupList[id].CharacteristicList != null)
						{
							var nameFile = groupList[id].ProductGroupTitle;
							var nameFileXlsx = nameFile + ".xlsx";
							var nameFile00 = nameFile + "00.xlsx";
							var nameFileAdditional = nameFile + "-ДопХ.xlsx";



							var file1 = directoryInfo.GetFiles().FirstOrDefault(c => c.Name.StartsWith($"{nameFile}.xlsx"));
							var file2 = directoryInfo.GetFiles().FirstOrDefault(c => c.Name.StartsWith($"{nameFile}00.xlsx"));
							var file3 = directoryInfo.GetFiles().FirstOrDefault(c => c.Name.StartsWith($"{nameFile}-ДопХ.xlsx"));

						

							List<Product> products = new List<Product>();

							var productGroupSub = new ProductGroupSub()
							{
								Title = nameFile,
								ProductCategory = new List<CategoryFromXls>(),
								Id = groupList[id].ProductGroupId,
								ProductCharacteristics = new List<ProductCharacteristic>(),
								Products = new List<Product>()
							};

							productGroupSub.ProductCategory = groupList[id].CategoryList;
							productGroupSub.ProductCharacteristics = groupList[id].CharacteristicList;
							productGroupSub.ParentCategoryId = groupList[id].ParentCategoryId;
							productGroupSub.ParentCategoryTitle = groupList[id].ParentCategoryTitle;					

							if (file1 != null)
							{	
								FileProcessing(id, nameFileXlsx, file3, products);
								productGroupSub.Products = products;
							}


							if (file2 != null)
							{
								FileProcessing(id, nameFile00, file3, products);
								productGroupSub.Products = products;
							}



							if (file3 != null)
							{
								FileProcessingAdditional(id, nameFileAdditional, file1, products, productGroupSub);
								productGroupSub.Products = products;
							}

							if (file1 !=null || file2 != null)
							productsReadyForUnloading.Add(productGroupSub);
							
						}
						

					}
					

					return View(groupList);

					//try
					//{
					//	List<Product> listProducts = new List<Product>();
					//	List<int> ListP = new List<int>();
					//	for (int row = 3; row <= range.Rows.Count; row++)
					//	{

					//		Product p = new Product();
					//		p.Level1 = (range.Cells[row, 1]).Text;
					//		p.Level2 = (range.Cells[row, 2]).Text;
					//		p.Level3 = (range.Cells[row, 3]).Text;
					//		p.Level4 = (range.Cells[row, 4]).Text;
					//		listProducts.Add(p);							
					//		if ((range.Cells[row, 1]).Text == "")
					//		{
					//			counter++;
					//		}
					//		else
					//		{
					//			if (counter != 0)
					//			{
									
					//				ListP.Add(counter);
					//				counter = 0;
									
					//			}
								

					//		}
					//		//ccc = (range.Cells[row, 1]).Text;


					//	}
					//	ViewBag.ListProducts = listProducts;
					//	ViewBag.Counter = counter;
					//	ViewBag.ListP = ListP;
					//}
					//catch
					//{
					//	ViewBag.Error = "Ошибка";
					//}
					
					//application.Workbooks.Close();
					//return View("Success");
				}
				else
				{
					return View();
				}
			}

		}

		
	}
}