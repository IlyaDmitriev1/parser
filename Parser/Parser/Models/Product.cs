﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace Parser.Models
{
	public class ProductGroup
	{
		public string ProductGroupTitle { get; set; }
		public string ProductGroupId { get; set; }
		public List<ProductCharacteristic> CharacteristicList { get; set; }
		public List<CategoryFromXls> CategoryList { get; set; }
		public List<Errorss> Errors { get; set; }
		public string IsManualLoading { get; set; }
		public string ParentCategoryId { get; set; }
		public string ParentCategoryTitle { get; set; }
	}
	public class ErrorTypes
	{
		public static string NoTitleInGroup { get; set; } = "Нет названия у группы";
		public static string NoIdInGroup { get; set; } = "Нет ID у группы";
		public static string NoCategoryInGroup { get; set; } = "Нет категории(рубрики) у группы";
		public static string NoIdInCharacteristic { get; set; } = "Нет ID у характеристики";
		public static string NoTitleInCharacteristic { get; set; } = "Нет Названия у характеристики";
		public static string NoIdInCategory { get; set; } = "Нет ID у категории";
		public static string NoTitleInCategory { get; set; } = "Нет названия у категории";
		public static string IsManualUploading { get; set; } = "Установлена ручная выргузка";
		public static string NoExistFile { get; set; } = "НЕТ ТАКОГО ФАЙЛА";
	}

	public class CategoryFromXls
	{
		public string Id { get; set; }
		public string Title { get; set; }
		public string ParentId { get; set; }

	}

	public class Errorss
	{
		public string Error { get; set; }
	}

	public class ProductCharacteristic
	{
		//файл реестра
		public string Id { get; set; }
		public string Title { get; set; }
		public bool IsAdditional { get; set; }

	}

	public class ProductGroupSub
	{
		public string Id { get; set; }
		public string Title { get; set; }
		public List<Product> Products { get; set; }
		public List<ProductCharacteristic> ProductCharacteristics { get; set; }
		public List<CategoryFromXls> ProductCategory { get; set; }
		public string ParentCategoryId { get; set; }
		public string ParentCategoryTitle { get; set; }
	}

	public class Product
	{
		// Обычный файл .xls
		public string VendorСode { get; set; }
		public string VolumeAmount { get; set; }
		public string VolumeAmountWithPrices { get; set; }
		public string NumberOfOffersTotal { get; set; }
		public string NumberOfOffersSimple { get; set; }
		public string PriceWeightedByFactor { get; set; }
		public string PriceAverageTakingIntoAccountCoeff { get; set; }
		public List<CharacteristicInFile> ProductCharacteristics { get; set; }
		public string ProductWasDownloadedFromFile00 { get; set; }
		// -ДопХарактеристики
		public string Volume { get; set; }
		public string NumberOfOffersAdditional { get; set; }

	}





	public class CharacteristicInFile
	{
		public string Title { get; set; }

		//файлы товарной группы
		public string productCharacteristics { get; set; }
	}
}